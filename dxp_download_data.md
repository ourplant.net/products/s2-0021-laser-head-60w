Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0021-laser-head-60w).

|document|download options|
|:-----|-----:|
|operating manual           ||
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0021-laser-head-60w/-/raw/main/02_assembly_drawing/s2-0021_a_znb_laser_head_60w.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s2-0021-laser-head-60w/-/raw/main/03_circuit_diagram/S2-0021-EPLAN-A.pdf)|
|maintenance instructions   ||
|spare parts                |[en](https://gitlab.com/ourplant.net/products/s2-0021-laser-head-60w/-/raw/main/05_spare_parts/S2-0021-A1-SWP-Laser.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
