Der Mergenthaler Laserlötkopf ist ein vielseitig einsetzbarer Bearbeitungskopf. Er kommt unter anderem bei Prozessen zum Einsatz, bei denen Standardlötverfahren nicht anwendbar sind.
Dies ist insbesondere bei temperaturempfindlichen sowie selektiv zu bestückenden Bauteilen und bei Bauteilen, die eine nachträgliche Kontaktierung erfordern, der Fall.
Der Kopf ist prädistiniert für das selektive Laserlöten und findet hauptsächlich Anwendung bei Prozessen im Bereich Optik, Medizintechnik und Automotive.
Der Laserkopf mit Zustellachse verfügt über eine integrierte Laserquelle, eine Optik sowie Prozessregelungs- und Beobachtungstechnik. Ein integriertes Pyrometer misst berührungslos die Temperatur an der Lötstelle.
<!-- 2021 (C) Häcker Automation GmbH -->